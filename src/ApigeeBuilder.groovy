import com.cloudbees.plugins.credentials.common.StandardUsernamePasswordCredentials
import com.cloudbees.plugins.credentials.CredentialsProvider
import hudson.util.Secret
def generateSteps(job, builddata, cl) {
    return builddata.collectEntries {
        ["${it.name} ${it.version}" : cl(it)]
    }
}
def prebuild(Object buildconfig) {
	return {
      buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "beforeBuild", Object)) {
               it.proj.beforeBuild(it)
           }
       }
    } // end return
}
	
def code_quality(Object buildconfig) {
        return {
       
            dir("${buildconfig.workingdir}") {
                sh """
                    echo "Nothing To Do"
                """
            }
        
    } // end return
}



def unittests(Object buildconfig) {
    return {
       buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "beforeTest", Object)) {
               it.proj.beforeTest(it)
           }
			  dir("${buildconfig.workingdir}") {
                sh """
                    echo "Nothing To Do"
                """
            }
		buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "afterTest", Object)) {
               it.proj.afterTest(it)
           }
       }
	 }
}
}


def build(Object buildconfig) {
    return {
        StandardUsernamePasswordCredentials artifactoryCreds = CredentialsProvider.findCredentialById("NEXUSID",
        StandardUsernamePasswordCredentials.class, currentBuild.rawBuild)
        def artifactoryUser = artifactoryCreds.getUsername() 
        def artifactoryPass = Secret.toString(artifactoryCreds.getPassword())
			buildconfig.jobs.each {
           dir("${buildconfig.workingdir}") {
              //  writeFile file: 'apigee-maven-settings.xml', text: libraryResource('apigee-maven-settings.xml')
                sh """
                  cd "${buildconfig.workingdir}/${it.pomDir}"
                  '${it.mvnName}/bin/mvn' -DskipTests clean package
                  
                """
            }
		}
        
    } // end return
}

def postbuild(Object buildconfig) {
     return {
        buildconfig.jobs.each {
           if (it.proj.metaClass.respondsTo(it.proj, "afterBuild", Object)) {
               it.proj.beforeBuild(it)
           }
       }
    } // end return
}

def publish(Object buildconfig) {
   return {
       buildconfig.jobs.each {
       if (it.proj.metaClass.respondsTo(it.proj, "beforePush", Object)) {
           it.proj.beforePush(it)
       }
     
               withCredentials([[

                                $class          : 'UsernamePasswordMultiBinding',
                                credentialsId   : "NEXUSID",
                                usernameVariable: 'ARTIFACTORY_USERID',
                                passwordVariable: 'ARTIFACTORY_PASSWORD'
                            ]]) {

               dir("${buildconfig.workingdir}/${it.pomDir}") {
                   sh """
                       cd target/apiproxy
                       mv *.zip ${it.name}.zip
                       cd ..
                   """

                   sh """
				   curl -v -u ${ARTIFACTORY_USERID}:${ARTIFACTORY_PASSWORD}  --upload-file target/apiproxy/${it.name}.zip http://${it.arttifactserver}/repository/testmaven/${it.repoName}/${it.branch}/${it.version}/${it.name}.zip
				  
                       """
                   buildconfig["build_artifact_${it.name}"] = "http://${it.arttifactserver}/repository/testmaven/${it.repoName}/${it.branch}/${it.version}/${it.name}.zip"
               }
           }    
       
        if (it.proj.metaClass.respondsTo(it.proj, "afterPush", Object)) {
           it.proj.afterPush(it)
       }
	   }
   } // end return
}

return this